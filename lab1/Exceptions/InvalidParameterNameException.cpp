//
// Created by dk on 09.09.17.
//

#include "InvalidParameterNameException.h"

InvalidParameterNameException::InvalidParameterNameException(const string &__arg) : invalid_argument(
        "Can't find \"" + __arg + "\" parameter") {}
