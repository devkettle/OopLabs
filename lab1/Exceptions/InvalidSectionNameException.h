//
// Created by dk on 09.09.17.
//

#ifndef LAB1_INVALIDSECTIONNAMEEXCEPTION_H
#define LAB1_INVALIDSECTIONNAMEEXCEPTION_H

#include <stdexcept>
#include <exception>

using namespace std;

class InvalidSectionNameException : invalid_argument {
public:
    InvalidSectionNameException(const string &__arg);

};


#endif //LAB1_INVALIDSECTIONNAMEEXCEPTION_H
