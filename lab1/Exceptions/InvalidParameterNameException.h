//
// Created by dk on 09.09.17.
//

#ifndef LAB1_INVALIDPARAMETERNAMEEXCEPTION_H
#define LAB1_INVALIDPARAMETERNAMEEXCEPTION_H

#include <stdexcept>
#include <exception>

using namespace std;

class InvalidParameterNameException : invalid_argument {
public:
    InvalidParameterNameException(const string &__arg);

};


#endif //LAB1_INVALIDPARAMETERNAMEEXCEPTION_H
