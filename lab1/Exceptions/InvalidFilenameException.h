//
// Created by dk on 09.09.17.
//

#ifndef LAB1_INVALIDFILENAMEEXCEPTION_H
#define LAB1_INVALIDFILENAMEEXCEPTION_H

#include <stdexcept>
#include <exception>

using namespace std;

class InvalidFilenameException : invalid_argument {
public:
    InvalidFilenameException(const string &__arg);

};


#endif //LAB1_INVALIDFILENAMEEXCEPTION_H
